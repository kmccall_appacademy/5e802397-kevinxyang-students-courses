class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(new_course)
    # return if course.students.include?(self)

    # adds course to student's list of courses
    @courses << new_course unless @courses.include?(new_course)

    # updates the course's list of students
    new_course.students << self
  end


  def course_load
    # returns a hash of department names pointing to # of credits
    courses_hash = Hash.new(0)

    self.courses.each do |course|
      courses_hash[course.department] += course.credits
    end

    courses_hash
  end

end
